public class FindMin {
    public static void main(String[] args){
        if (args.length == 3 ){
            int number1 = Integer.parseInt(args[0]);
            int number2 = Integer.parseInt(args[1]);
            int number3 = Integer.parseInt(args[2]);
            System.out.println("First number : " + number1 + " Second number: " + number2 + " Third number: " +number3);

            int min = number1 < number2 ? number1:number2;
            min = min < number3 ? min: number3;
            System.out.println("Min: "+min);


        }

    }
}
